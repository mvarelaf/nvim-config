" vim: foldmethod=marker

if empty(glob(stdpath('config').'/autoload/plug.vim'))
  silent execute '!curl -fLo '.stdpath('config').'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  " autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin(stdpath('data').'/bundle')
Plug 'tpope/vim-sensible'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-unimpaired'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-commentary', !has('nvim-0.10') ? {} : { 'on': [] }
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-sleuth'
Plug 'mattn/calendar-vim'
Plug 'mhinz/vim-startify'
Plug 'ackyshake/VimCompletesMe', { 'frozen': 1 }
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'
Plug 'crispgm/telescope-heading.nvim'
Plug 'kyoh86/telescope-windows.nvim'
Plug 'junegunn/vim-easy-align'
Plug 'dhruvasagar/vim-table-mode'
Plug 'preservim/tagbar'
Plug 'nvim-treesitter/nvim-treesitter'
Plug 'nvim-orgmode/orgmode'
Plug 'kevinhwang91/nvim-bqf'
Plug 'sindrets/diffview.nvim'
Plug 'https://gitlab.com/protesilaos/tempus-themes-vim.git', { 'frozen': 1 }
Plug 'sainnhe/everforest'
Plug 'habamax/vim-nod'
Plug 'mg979/vim-xtabline'

call plug#end()

if has("multi_byte")
  if &termencoding == ""
    let &termencoding = &encoding
  endif
  set encoding=utf-8             " better default than latin1
  setglobal fileencoding=utf-8   " change default file encoding when writing new files
endif

let mapleader = ","
let maplocalleader = ","

set cmdheight=2

set showcmd
set hidden

set hlsearch
set ignorecase
set smartcase
set infercase

set smartindent

set clipboard=unnamed "copying from others with p, instead of "*p

set matchpairs=(:),{:},[:],<:> "for use with % key
set showmatch "show matching matchpair after completion

set virtualedit=block

set shortmess+=c    " don't give |ins-completion-menu| messages
set shortmess+=I    " no intro message
set shortmess+=r    " use "[RO]" instead of "[readonly]"
set shortmess+=m    " use "[+]" instead of "[Modified]"
set shortmess-=S    " show search count message when searching, e.g. [1/5]"
set shortmess+=w    " w use "[w]" instead of "written" for file write message
                    " and "[a]" instead of "appended" for ':w >> file' command
if !has('nvim-0.10')
  set shortmess+=n  " n use "[New]" instead of "[New File]"
  set shortmess+=x  " x use "[dos]" instead of "[dos format]", "[unix]" instead of
                    "[unix format]" and "[mac]" instead of "[mac format]".
endif

set suffixes+=.pyc,.pyo,.egg-info,.class

if has("nvim-0.4.0")
  set wildoptions=pum,fuzzy
  cnoremap <expr> <Up> wildmenumode() ? '<Left>' : '<Up>'
  cnoremap <expr> <Down> wildmenumode() ? '<Right>' : '<Down>'
  cnoremap <expr> <Left> wildmenumode() ? '<Up>' : '<Left>'
  cnoremap <expr> <Right> wildmenumode() ? '<Down>' : '<Right>'
else
  set wildmode=longest:list,full
endif

if has('wildignore')
  set wildignorecase
  set wildignore=*.o,*.obj,*.bak,*.exe,*.swp,*~,*.tmp
  set wildignore+=.git,.hg,.svn
  " set wildignore+=*.bmp,*.gif,*.jpg,*.png
  set wildignore+=*.aux,*.out,*.toc        " LaTeX intermediate files
  set wildignore+=.DS_Store                " Mac
  set wildignore+=*.a,*.pdb,*.lib "stuff to ignore when tab completing
  set wildignore+=__pycache__,.stversions,*.spl,*.out,%*
  set wildignore+=*.so,*.dll,*.egg,*.jar,*.class,*.pyc,*.pyo,*.bin,*.dex
  set wildignore+=*.zip,*.7z,*.rar,*.gz,*.tar,*.gzip,*.bz2,*.tgz,*.xz
  set wildignore+=*.ipch,*.gem
  set wildignore+=*.ico,*.tga,*.pcx,*.ppm,*.img,*.iso
  set wildignore+=*/.Trash/**,*.dmg,*/.rbenv/**
  set wildignore+=*.wav,*.mp3,*.ogg,*.pcm
  set wildignore+=*.mht,*.suo,*.sdf,*.jnlp
  " set wildignore+=*.chm,*.epub,*.pdf,*.mobi,*.ttf
  set wildignore+=*.mp4,*.avi,*.flv,*.mov,*.mkv,*.swf,*.swc
  " set wildignore+=*.ppt,*.pptx,*.docx,*.xlt,*.xls,*.xlsx,*.odt,*.wps
  set wildignore+=*.msi,*.crx,*.deb,*.vfd,*.apk,*.ipa,*.msu
  set wildignore+=*.gba,*.sfc,*.078,*.nds,*.smd,*.smc
  set wildignore+=*.linux2,*.win32,*.darwin,*.freebsd,*.linux,*.android
endif

set nojoinspaces " Use only 1 space after "." when joining lines, not 2

" if has('insert_expand')
"   " set completeopt=menuone,noinsert,noselect
"   set completeopt=menuone,preview "noselect
" endif

if has('mksession')
  set sessionoptions-=blank
endif

set splitright
set splitbelow

" In many terminal emulators the mouse works just fine, thus enable it.
if has('mouse')
  set mouse=a
endif

" ignore whitespace in diff mode
set diffopt+=iwhite,vertical
if has("patch-8.1.0360") "from chrisbra/vim-diff-enhanced
  set diffopt+=algorithm:patience "myers is default algorithm
  set diffopt+=indent-heuristic
endif

if has('linebreak')
  set linebreak
  set showbreak=>
  set cpoptions+=n " 'showbreak' to appear in between line numbers
  set breakindent
  set breakindentopt=shift:3,sbr
endif

set lazyredraw

set noerrorbells
set visualbell t_vb=
set novisualbell
if exists('&belloff')
  set belloff=all
endif

set number
set relativenumber

if has("nvim-0.5.0") || has("patch-8.1.1564")
  " Recently vim can merge signcolumn and number column into one
  set signcolumn=number
else
  set signcolumn=yes
endif

set fillchars=fold:\ ,stlnc:\—

set nostartofline

if has('vcon')
  set termguicolors
endif

"https://vimways.org/2018/formatting-lists-with-vim/
if v:version > 703 || v:version == 703 && has('patch541')
  set formatoptions+=n " When formatting text, recognize numbered lists
  set formatlistpat=^\\s*                     " Optional leading whitespace
  set formatlistpat+=[                        " Start character class
  set formatlistpat+=\\[({]\\?                " |  Optionally match opening punctuation
  set formatlistpat+=\\(                      " |  Start group
  set formatlistpat+=[0-9]\\+                 " |  |  Numbers
  "set formatlistpat+=\\\|                     " |  |  or
  "set formatlistpat+=[a-zA-Z]\\+              " |  |  Letters
  set formatlistpat+=\\)                      " |  End group
  set formatlistpat+=[\\]:.)}                 " |  Closing punctuation
  set formatlistpat+=]                        " End character class
  set formatlistpat+=\\s\\+                   " One or more spaces
  set formatlistpat+=\\\|                     " or
  set formatlistpat+=^\\s*[-–+o*•]\\s\\+      " Bullet points
endif

" Add full path and buffer number to Ctrl-G display
nnoremap <C-g> 2<C-g>

" Highlight all occurrences of current word without moving
nnoremap <leader>* :let @/='\<<C-R>=expand("<cword>")<CR>\>'<CR>:set hls<CR>

" Open files located in the same dir in with the current file is edited
" nnoremap <leader>ecd :e <C-R>=expand("%:.:h") . "/"<CR>
nnoremap <leader>e :e <C-R>=(expand("%:.:h")==".")?"":expand("%:.:h")."/"<CR>
" 'cd' towards the directory in which the current file is edited
" but only change the path for the current window
nnoremap <leader>lcd :lcd %:h<CR>

" Poor man bufexplorer
nnoremap <leader>ls :ls<CR>:b<space>

" https://github.com/jwhitley/vim-preserve
function! Preserve(command)
  let l:saved_winview = winsaveview()
  let l:last_search = getreg('/')
  execute 'keepjumps ' . a:command
  call winrestview(l:saved_winview)
  call setreg('/', l:last_search)
endfunction

" Remove trailing whitespace
nnoremap <silent> <S-F1> :call Preserve("%s/\\s\\+$//e")<CR>

vnoremap <silent> <S-F1> :retab<CR>

" Visual select last paste
nnoremap <expr> vp '`[' . getregtype()[0] . '`]'

" https://vim.fandom.com/wiki/Smart_home
noremap <expr> <Home> (col('.') == matchend(getline('.'), '^\s*')+1 ? '0' : '^')
imap <Home> <C-o><Home>

"Inspired by https://vim.fandom.com/wiki/Find_in_files_within_Vim
func Eatchar(pat)
  let c = nr2char(getchar(0))
  return (c =~ a:pat) ? '' : c
endfunc

noremap <F3> :vimgrep //j *<C-Left><C-Left><Right><C-R>=Eatchar('\s')<CR>

noremap <S-F3> :vimgrep /\<lt><C-R>=expand("<cword>")<CR>\>/j *<C-Left><C-Left><Right><Right><Right><C-R>=Eatchar('\s')<CR>

noremap <A-F3> :vimgrep //j *<C-R>=(expand("%:e")=="" ? "" : ".".expand("%:e"))<CR> <C-Left><C-Left><Right><C-R>=Eatchar('\s')<CR>

noremap <A-S-F3> :vimgrep /\<lt><C-R>=expand("<cword>")<CR>\>/j *<C-R>=(expand("%:e")=="" ? "" : ".".expand("%:e"))<CR> <C-Left><C-Left><Right><Right><Right><C-R>=Eatchar('\s')<CR>

" enhance search with <space> as "whatever"
" to enter literal <space> use <C-Q><space>
cnoremap <expr> <space> getcmdtype() =~ '[/?]' ? '.\{-}' : "<space>"

" Too many mistakes
cabbrev W   w
cabbrev Q   q
cabbrev Qa  qa
cabbrev QA  qa
cabbrev Wq  wq
cabbrev WQ  wq
cabbrev Wqa wqa
cabbrev WQa wqa
cabbrev WQA wqa

if has('syntax')
  set spelllang=es,en
  set spelloptions=camel
endif

if executable("rg")
    set grepprg=rg\ --vimgrep\ --no-heading\ --smart-case
    set grepformat=%f:%l:%c:%m,%f:%l:%m
endif

if has('autocmd')
  augroup fileTypes
  autocmd!
  autocmd FileType text setlocal textwidth=79 syntax=txt
  autocmd FileType text setlocal commentstring=/*%s*/ " used by commentary.vim
  if has('file_in_path')
    autocmd FileType markdown setlocal suffixesadd=.md,.markdown
  endif
  autocmd FileType racket setlocal commentstring=;;%s
  autocmd FileType c,cpp setlocal cinoptions=:0,l1,t0,g0,(0
  augroup END

  augroup vimrcEx
  autocmd!
  " When editing a file, always jump to the last known cursor position.
  " Don't do it when the position is invalid or when inside an event handler
  " (happens when dropping a file on gvim).
  " Also don't do it when the mark is in the first line, that is the default
  " position when opening a file.
  autocmd BufReadPost *
    \ if line("'\"") > 1 && line("'\"") <= line("$") |
    \   exe "normal! g`\"" |
    \ endif

  " The following autocommand will cause the quickfix window to open after any grep invocation:
  autocmd QuickFixCmdPost [^l]*grep* cwindow
  autocmd QuickFixCmdPost l*grep* lwindow
  augroup END

  if !&diff
    " Instead of https://github.com/mtth/cursorcross.vim
    augroup cursor_line
      autocmd!
      au VimEnter * set cursorline
      au WinLeave,InsertEnter * set nocursorline
      au WinEnter,InsertLeave * set cursorline
    augroup END
  endif

  augroup niceties
    autocmd!
    autocmd VimResized * wincmd =
  augroup END
endif

"set statusline=%{tabpagewinnr(tabpagenr())}\ %<%f\ %h%#User3#%m%*%r%=%{FugitiveHead(8)}\ %-14.(%l,%c%V%)%P

" Using winbar
"set statusline=%<%f\ %h%#User3#%m%*%r%=%{FugitiveHead(8)}\ %-14.(%l,%c%V%)%P
"set winbar=%=%{tabpagewinnr(tabpagenr())}:\ %<%f

runtime! mywin.vim
runtime! tabline.vim
runtime! statusline.vim
" runtime! colored_statusline.vim

language ctype es_ES.UTF-8
language messages es_ES.UTF-8
language time es_ES.UTF-8

"" Abbreviations {{{
iab _hoy <C-R>=strftime("%d.%m.%Y")<CR><C-R>=Eatchar('\s')<CR>
iab _time <C-R>=strftime("%H:%M:%S")<CR><C-R>=Eatchar('\s')<CR>
iab _dates <C-R>=strftime("%d/%m/%Y %H:%M")<CR><C-R>=Eatchar('\s')<CR>
iab _fecha <C-R>=strftime("%a %b %d %T %Z %Y")<CR><C-R>=Eatchar('\s')<CR>
iab _date <C-R>=strftime("%A %d %B %Y %H:%M")<CR><C-R>=Eatchar('\s')<CR>
"" }}}

" Do not load some standard plugins
let g:loaded_getscriptPlugin = 1
let g:loaded_vimballPlugin = 1
let g:loaded_logiPat = 1
let g:loaded_rrhelper = 1
let g:loaded_gzip = 1
let g:loaded_zipPlugin = 1
let g:loaded_tarPlugin = 1
" let g:loaded_spellfile_plugin = 1
" let g:loaded_2html_plugin = 1
" let g:loaded_netrwPlugin = 1
" let loaded_matchparen = 1

if has('nvim')
  if has("nvim-0.6.0")
    " https://github.com/neovim/neovim/issues/15426
    if maparg('Y', 'n') != ''
      unmap Y
    endif
  endif

  if has("nvim-0.9.0")
    set diffopt+=linematch:60
  endif

  set title
  set titlestring=%f

  " preview effects of a substitute command
  set inccommand=split

  let g:loaded_tutor_mode_plugin = 1  " do not load the tutor plugin

  " Set up cursor color and shape in various mode, ref:
  " https://github.com/neovim/neovim/wiki/FAQ#how-to-change-cursor-color-in-the-terminal
  " set guicursor=n-v-c-sm:block,i-ci-ve:ver25,r-cr-o:hor20 "Nvim Default
  set guicursor=n-v-c-sm:block-Cursor/lCursor,ci-ve:ver50-Cursor2/lCursor2,r-cr-o:hor20,i:ver25-blinkwait300-blinkoff300-blinkon300-Cursor/lCursor
  "El blink no funciona en Nvy

  augroup term_settings
    autocmd!
    " Do not use number and relative number for terminal inside nvim
    autocmd TermOpen * setlocal norelativenumber nonumber
    " Go to insert mode by default to start typing command
    autocmd TermOpen * startinsert
  augroup END

  " https://github.com/iamcco/dotfiles/blob/master/nvim/viml/neovim.vim
  function s:exit_to_normal() abort
    if &filetype ==# 'fzf'
      return "\<Esc>"
    endif
    return "\<C-\>\<C-n>"
  endfunction
  tnoremap <expr> <Esc> <SID>exit_to_normal()

  augroup gui_load
    autocmd!
    autocmd UIEnter * runtime! ginit.vim
  augroup END

endif

nnoremap <silent> <leader>c :close<cr>
nnoremap <silent> <leader>q :quit<cr>

let g:markdown_folding = 1

"" PLUGINS
"" CALENDAR https://github.com/mattn/calendar-vim {{{
let g:calendar_no_mappings = 1
let g:calendar_monday = 1
let g:calendar_weeknm = 2 " WK 1
let g:calendar_mark = 'left-fit'
let g:calendar_number_of_months = 6
if has('autocmd')
  augroup calendar
  autocmd!
  autocmd Filetype calendar setlocal nonumber norelativenumber
  augroup END
endif
"" }}}

" TAGBAR https://github.com/preservim/tagbar
" https://github.com/majutsushi/tagbar
nmap <F12> :TagbarToggle<CR>

" VIM-EASY-ALIGN https://github.com/junegunn/vim-easy-align
" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)
" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

xmap <Leader>ga <Plug>(LiveEasyAlign)

" STARTIFY https://github.com/mhinz/vim-startify
" .\after\plugin\startify.vim
nnoremap <silent> <leader>s :split<bar>Startify<cr>
nnoremap <silent> <leader>v :vnew<bar>Startify<cr>
nnoremap <silent> <leader>t :tabnew<bar>Startify<cr>

"" VIM-XTABLINE https://github.com/mg979/vim-xtabline
let g:xtabline_settings = get(g:, 'xtabline_settings', {})
let g:xtabline_settings.enable_mappings = 0
let g:xtabline_settings.show_right_corner = 0

" TELESCOPE https://github.com/nvim-telescope/telescope.nvim
" Using Lua functions
nnoremap <leader>f <cmd>lua require('telescope.builtin').find_files()<cr>
nnoremap <leader>gg <cmd>lua require('telescope.builtin').live_grep()<cr>
nnoremap <leader>b <cmd>lua require('telescope.builtin').buffers()<cr>
nnoremap <leader>h <cmd>lua require('telescope.builtin').help_tags()<cr>
nnoremap <leader>o <cmd>lua require('telescope.builtin').oldfiles()<cr>
nnoremap <leader>r <cmd>lua require('telescope.builtin').registers()<cr>
nnoremap <leader>m <cmd>lua require('telescope.builtin').marks()<cr>
nnoremap <leader>º <cmd>lua require('telescope.builtin').command_history()<cr>
nnoremap <leader>ª <cmd>lua require('telescope.builtin').search_history()<cr>
nnoremap <leader>j <cmd>lua require('telescope.builtin').jumplist()<cr>
nnoremap <leader>+ <cmd>lua require('telescope.builtin').current_buffer_fuzzy_find()<cr>

nnoremap <leader>ñ <cmd>lua require('telescope.builtin').colorscheme()<cr>
nnoremap <leader>gq <cmd>lua require('telescope.builtin').quickfixhistory()<cr>
nnoremap <leader>gs <cmd>lua require('telescope.builtin').git_status()<cr>
nnoremap <leader>gl <cmd>lua require('telescope.builtin').git_branches()<cr>
nnoremap <leader>gb <cmd>lua require('telescope.builtin').builtin()<cr>
nnoremap <leader>gt <cmd>lua require('telescope.builtin').treesitter()<cr>
nnoremap <leader><leader> <cmd>lua require('telescope.builtin').resume()<cr>

nnoremap <leader>- <cmd>Telescope heading<cr>
nnoremap <leader>w <cmd>Telescope windows<cr>

lua <<EOF
-- require 'colorizer'.setup()

telescope = require('telescope')
telescope.setup({
  defaults = {
    mappings = {
      i = {
        ["<esc>"] = require('telescope.actions').close,
      },
    },
  },
  pickers = {
    buffers = {
      previewer = false,
      theme = "dropdown",
    },
    oldfiles = {
      previewer = false,
    },
    find_files = {
      previewer = false,
      theme = "ivy",
    },
    jumplist = {
      trim_text = true,
      fname_width = 20,
      path_display = {"tail"},
      theme = "dropdown",
    },
    current_buffer_fuzzy_find = {
      skip_empty_lines = true,
      sorting_strategy = "ascending",
      layout_config = {
        prompt_position = "top"
      },
    },
  },
  extensions = {
    heading = {
      treesitter = true,
      picker_opts = {
        previewer = false,
        sorting_strategy = "ascending",
        layout_strategy = "vertical",
        layout_config = {
          prompt_position = "top"
        },
      },
    },
  },
})

-- Tree-sitter configuration
require'nvim-treesitter.configs'.setup {
  -- If TS highlights are not enabled at all, or disabled via `disable` prop, highlighting will fallback to default Vim syntax highlighting
  highlight = {
    enable = true,
  },
  -- https://github.com/nvim-treesitter/nvim-treesitter/issues/3092
  ensure_installed = {"c", "lua", "vim", "vimdoc", "query", "markdown", "markdown_inline" }, -- Or run :TSUpdate org
  indent = {
    enable = true,
  },
  incremental_selection = {
    enable = true,
  },
}

require('orgmode').setup({
  org_startup_indented = true,
  org_agenda_files = {'~/Documents/notas/**/*.org'},
  org_default_notes_file = '~/Documents/notas/inbox.org',
  org_todo_keywords = {'TODO(t)', 'WAITING(w)', 'HOLD(h)', 'INPROGRESS(i)', 'BLOCKED(b)', 'SOMEDAY(y)', '|', 'DONE(d)', 'CANCELLED(c)'},
  org_startup_folded = 'inherit',
  org_log_done = 'note',
  org_log_into_drawer = 'LOGBOOK',
})

-- use meta_return also in insert mode
vim.api.nvim_create_autocmd('FileType', {
  pattern = 'org',
  callback = function()
    vim.keymap.set('i', '<S-CR>', '<cmd>lua require("orgmode").action("org_mappings.meta_return")<CR>', {
      silent = true,
      buffer = true,
    })
  end,
})

require('bqf').setup({
  preview = { auto_preview = false }
})

EOF

if has('nvim-0.10')
  if has('gui_running')
    "colorscheme tempus_classic "dark
    "colorscheme tempus_dawn "light

    let g:everforest_dim_inactive_windows = 1
    let g:everforest_cursor = 'orange'
    "let g:everforest_ui_contrast = 'high'
    colorscheme everforest
  else
    colorscheme default
  endif
else
  try
    colorscheme habamax "Included into vim 9.0.133
  catch /^Vim\%((\a\+)\)\=:E185:/
    colorscheme desert "dark
    " colorscheme delek "light
  endtry
endif
