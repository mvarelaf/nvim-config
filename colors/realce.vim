" Adapted from https://github.com/igemnace/highlight.vim
" :%s/#303030/#242424/gc
" add TextYellowU for Todo group
" add definitions or links for missing groups
" do not clear Italic

" vimscript9 utils
":source $VIMRUNTIME/colors/tools/check_colors.vim
":source $VIMRUNTIME/syntax/hitest.vim

" Author: Ian Emnace <igemnace@gmail.com>
" Palette:
" black      236  #303030
" red        210  #ff8787
" green      114  #87d787
" yellow     221  #ffd75f
" blue       68   #5f87d7
" magenta    176  #d787d7
" cyan       80   #5fd7d7
" white      252  #d0d0d0
" lightgray  243  #767676
" darkgray   237  #3a3a3a

""" COLORSCHEME BOILERPLATE {{{
set background=dark
highlight clear
if exists('syntax_on')
  syntax reset
endif
let g:colors_name = 'realce'
""" END COLORSCHEME BOILERPLATE }}}

""" ENVIRONMENT-DEPENDENT HIGHLIGHTS {{{
" Allows for graceful degradation:
" 1. Exact hex colors for GUI and termguicolors
" 2. Xterm palette equivalent for 256-color terminals
" 3. ANSI color equivalent for 16-color terminals
" See: https://github.com/romainl/vim-rnb and the logic behind it

if $TERM =~ '256' || &t_Co >= 256 || has('gui_running')
  """ FULL-FEATURED {{{
  " Xterm palette for cterm, hex for GUI

  " Syntax groups: convenient highlight groups for syntax highlighting
  highlight Standard       cterm=NONE       ctermfg=252  ctermbg=NONE  gui=NONE       guifg=#d0d0d0  guibg=NONE     guisp=NONE
  highlight Normal         cterm=NONE       ctermfg=252  ctermbg=236   gui=NONE       guifg=#d0d0d0  guibg=#242424  guisp=NONE
  highlight TextRed        cterm=NONE       ctermfg=210  ctermbg=NONE  gui=NONE       guifg=#ff8787  guibg=NONE     guisp=NONE
  highlight TextGreen      cterm=NONE       ctermfg=114  ctermbg=NONE  gui=NONE       guifg=#87d787  guibg=NONE     guisp=NONE
  highlight TextYellow     cterm=NONE       ctermfg=221  ctermbg=NONE  gui=NONE       guifg=#ffd75f  guibg=NONE     guisp=NONE
  highlight TextYellowU    cterm=underline  ctermfg=221  ctermbg=NONE  gui=underline  guifg=#ffd75f  guibg=NONE     guisp=#ffd75f
  highlight TextBlue       cterm=NONE       ctermfg=68   ctermbg=NONE  gui=NONE       guifg=#5f87d7  guibg=NONE     guisp=NONE
  highlight TextMagenta    cterm=NONE       ctermfg=176  ctermbg=NONE  gui=NONE       guifg=#d787d7  guibg=NONE     guisp=NONE
  highlight TextCyan       cterm=NONE       ctermfg=80   ctermbg=NONE  gui=NONE       guifg=#5fd7d7  guibg=NONE     guisp=NONE
  highlight TextWhite      cterm=NONE       ctermfg=252  ctermbg=NONE  gui=NONE       guifg=#d0d0d0  guibg=NONE     guisp=NONE
  highlight TextLightGray  cterm=NONE       ctermfg=243  ctermbg=NONE  gui=NONE       guifg=#767676  guibg=NONE     guisp=NONE
  highlight TextDarkGray   cterm=NONE       ctermfg=237  ctermbg=NONE  gui=NONE       guifg=#3a3a3a  guibg=NONE     guisp=NONE

  " User highlight groups: useful for statusline, where the background is dark gray
  highlight User1  cterm=NONE  ctermfg=1  ctermbg=237  gui=NONE  guifg=#ff8787  guibg=#3a3a3a  guisp=NONE
  highlight User2  cterm=NONE  ctermfg=2  ctermbg=237  gui=NONE  guifg=#87d787  guibg=#3a3a3a  guisp=NONE
  highlight User3  cterm=NONE  ctermfg=3  ctermbg=237  gui=NONE  guifg=#ffd75f  guibg=#3a3a3a  guisp=NONE
  highlight User4  cterm=NONE  ctermfg=4  ctermbg=237  gui=NONE  guifg=#5f87d7  guibg=#3a3a3a  guisp=NONE
  highlight User5  cterm=NONE  ctermfg=5  ctermbg=237  gui=NONE  guifg=#d787d7  guibg=#3a3a3a  guisp=NONE
  highlight User6  cterm=NONE  ctermfg=6  ctermbg=237  gui=NONE  guifg=#5fd7d7  guibg=#3a3a3a  guisp=NONE
  highlight User7  cterm=NONE  ctermfg=7  ctermbg=237  gui=NONE  guifg=#d0d0d0  guibg=#3a3a3a  guisp=NONE

  " Editor chrome
  highlight Bold              cterm=bold       ctermfg=NONE  ctermbg=NONE  gui=bold       guifg=NONE     guibg=NONE     guisp=NONE
  highlight ColorColumn       cterm=NONE       ctermfg=NONE  ctermbg=237   gui=NONE       guifg=NONE     guibg=#3a3a3a  guisp=NONE
  highlight Conceal           cterm=NONE       ctermfg=68    ctermbg=236   gui=NONE       guifg=#5f87d7  guibg=#242424  guisp=NONE
  highlight Cursor            cterm=NONE       ctermfg=236   ctermbg=252   gui=NONE       guifg=#242424  guibg=#d0d0d0  guisp=NONE
  highlight CursorColumn      cterm=NONE       ctermfg=NONE  ctermbg=237   gui=NONE       guifg=NONE     guibg=#3a3a3a  guisp=NONE
  highlight CursorLine        cterm=NONE       ctermfg=NONE  ctermbg=237   gui=NONE       guifg=NONE     guibg=#3a3a3a  guisp=NONE
  highlight CursorLineNr      cterm=NONE       ctermfg=243   ctermbg=236   gui=NONE       guifg=#767676  guibg=#242424  guisp=NONE
  highlight Debug             cterm=NONE       ctermfg=210   ctermbg=NONE  gui=NONE       guifg=#ff8787  guibg=NONE     guisp=NONE
  highlight DiffAdd           cterm=bold       ctermfg=114   ctermbg=237   gui=bold       guifg=#87d787  guibg=#3a3a3a  guisp=NONE
  highlight DiffChange        cterm=NONE       ctermfg=243   ctermbg=237   gui=NONE       guifg=#767676  guibg=#3a3a3a  guisp=NONE
  highlight DiffDelete        cterm=NONE       ctermfg=210   ctermbg=237   gui=NONE       guifg=#ff8787  guibg=#3a3a3a  guisp=NONE
  highlight DiffText          cterm=bold       ctermfg=221   ctermbg=237   gui=bold       guifg=#ffd75f  guibg=#3a3a3a  guisp=NONE
  highlight Directory         cterm=NONE       ctermfg=68    ctermbg=NONE  gui=NONE       guifg=#5f87d7  guibg=NONE     guisp=NONE
  highlight EndOfBuffer       cterm=NONE       ctermfg=243   ctermbg=NONE  gui=NONE       guifg=#767676  guibg=NONE     guisp=NONE
  highlight Error             cterm=NONE       ctermfg=236   ctermbg=210   gui=NONE       guifg=#242424  guibg=#ff8787  guisp=NONE
  highlight ErrorMsg          cterm=NONE       ctermfg=210   ctermbg=236   gui=NONE       guifg=#ff8787  guibg=#242424  guisp=NONE
  highlight FoldColumn        cterm=NONE       ctermfg=243   ctermbg=236   gui=NONE       guifg=#767676  guibg=#242424  guisp=NONE
  highlight Folded            cterm=NONE       ctermfg=243   ctermbg=237   gui=NONE       guifg=#767676  guibg=#3a3a3a  guisp=NONE
  highlight Ignore            cterm=NONE       ctermfg=236   ctermbg=NONE  gui=NONE       guifg=#242424  guibg=NONE     guisp=NONE
  highlight IncSearch         cterm=NONE       ctermfg=237   ctermbg=68    gui=bold       guifg=#3a3a3a  guibg=#5f87d7  guisp=NONE
  highlight LineNr            cterm=NONE       ctermfg=243   ctermbg=236   gui=NONE       guifg=#767676  guibg=#242424  guisp=NONE
  highlight Macro             cterm=NONE       ctermfg=210   ctermbg=NONE  gui=NONE       guifg=#ff8787  guibg=NONE     guisp=NONE
  highlight MatchParen        cterm=NONE       ctermfg=NONE  ctermbg=237   gui=NONE       guifg=NONE     guibg=#3a3a3a  guisp=NONE
  highlight ModeMsg           cterm=bold       ctermfg=114   ctermbg=NONE  gui=bold       guifg=#87d787  guibg=NONE     guisp=NONE
  highlight MoreMsg           cterm=NONE       ctermfg=114   ctermbg=NONE  gui=NONE       guifg=#87d787  guibg=NONE     guisp=NONE
  highlight NonText           cterm=NONE       ctermfg=243   ctermbg=NONE  gui=NONE       guifg=#767676  guibg=NONE     guisp=NONE
  highlight Pmenu             cterm=NONE       ctermfg=243   ctermbg=237   gui=NONE       guifg=#767676  guibg=#3a3a3a  guisp=NONE
  highlight PmenuSbar         cterm=NONE       ctermfg=NONE  ctermbg=237   gui=NONE       guifg=NONE     guibg=#3a3a3a  guisp=NONE
  highlight PmenuSel          cterm=NONE       ctermfg=221   ctermbg=237   gui=NONE       guifg=#ffd75f  guibg=#3a3a3a  guisp=NONE
  highlight PmenuThumb        cterm=NONE       ctermfg=NONE  ctermbg=243   gui=NONE       guifg=NONE     guibg=#767676  guisp=NONE
  highlight PmenuKind         cterm=NONE       ctermfg=221   ctermbg=243   gui=NONE       guifg=#ffd75f  guibg=#767676  guisp=NONE
  highlight PmenuKindSel      cterm=NONE       ctermfg=114   ctermbg=243   gui=NONE       guifg=#87d787  guibg=#767676  guisp=NONE
  highlight Question          cterm=NONE       ctermfg=68    ctermbg=NONE  gui=NONE       guifg=#5f87d7  guibg=NONE     guisp=NONE
  highlight QuickFixLine      cterm=underline  ctermfg=NONE  ctermbg=NONE  gui=underline  guifg=NONE     guibg=NONE     guisp=#d0d0d0
  highlight Search            cterm=NONE       ctermfg=237   ctermbg=243   gui=NONE       guifg=#3a3a3a  guibg=#767676  guisp=NONE
  highlight SignColumn        cterm=NONE       ctermfg=237   ctermbg=236   gui=NONE       guifg=#3a3a3a  guibg=#242424  guisp=NONE
  highlight SpecialKey        cterm=NONE       ctermfg=243   ctermbg=NONE  gui=NONE       guifg=#767676  guibg=NONE     guisp=NONE
  highlight SpellBad          cterm=underline  ctermfg=252   ctermbg=236   gui=underline  guifg=#d0d0d0  guibg=#242424  guisp=#d0d0d0
  highlight SpellCap          cterm=NONE       ctermfg=NONE  ctermbg=236   gui=NONE       guifg=NONE     guibg=#242424  guisp=NONE
  highlight SpellLocal        cterm=NONE       ctermfg=NONE  ctermbg=236   gui=NONE       guifg=NONE     guibg=#242424  guisp=NONE
  highlight SpellRare         cterm=NONE       ctermfg=NONE  ctermbg=236   gui=NONE       guifg=NONE     guibg=#242424  guisp=NONE
  highlight StatusLine        cterm=bold       ctermfg=252   ctermbg=243   gui=bold       guifg=#242424  guibg=#767676  guisp=NONE
  highlight StatusLineNC      cterm=NONE       ctermfg=243   ctermbg=237   gui=NONE       guifg=#767676  guibg=#3b3b3b  guisp=NONE
  highlight StatusLineTerm    cterm=NONE       ctermfg=243   ctermbg=237   gui=NONE       guifg=#767676  guibg=#3a3a3a  guisp=NONE
  highlight StatusLineTermNC  cterm=NONE       ctermfg=243   ctermbg=237   gui=NONE       guifg=#767676  guibg=#3b3b3b  guisp=NONE
  highlight TabLine           cterm=NONE       ctermfg=243   ctermbg=237   gui=NONE       guifg=#767676  guibg=#3a3a3a  guisp=NONE
  highlight TabLineFill       cterm=NONE       ctermfg=237   ctermbg=237   gui=NONE       guifg=#3a3a3a  guibg=#3a3a3a  guisp=NONE
  highlight TabLineSel        cterm=NONE       ctermfg=221   ctermbg=237   gui=NONE       guifg=#ffd75f  guibg=#3a3a3a  guisp=NONE
  highlight Title             cterm=NONE       ctermfg=68    ctermbg=NONE  gui=NONE       guifg=#5f87d7  guibg=NONE     guisp=NONE
  highlight TooLong           cterm=NONE       ctermfg=210   ctermbg=NONE  gui=NONE       guifg=#ff8787  guibg=NONE     guisp=NONE
  highlight Underlined        cterm=underline  ctermfg=210   ctermbg=NONE  gui=underline  guifg=#ff8787  guibg=NONE     guisp=#ff8787
  highlight VertSplit         cterm=NONE       ctermfg=236   ctermbg=236   gui=NONE       guifg=#242424  guibg=#303030  guisp=NONE
  highlight Visual            cterm=NONE       ctermfg=NONE  ctermbg=237   gui=NONE       guifg=NONE     guibg=#3a3a3a  guisp=NONE
  highlight VisualNOS         cterm=NONE       ctermfg=210   ctermbg=NONE  gui=NONE       guifg=#ff8787  guibg=NONE     guisp=NONE
  highlight WarningMsg        cterm=NONE       ctermfg=210   ctermbg=NONE  gui=NONE       guifg=#ff8787  guibg=NONE     guisp=NONE
  highlight WildMenu          cterm=NONE       ctermfg=221   ctermbg=237   gui=NONE       guifg=#ffd75f  guibg=#3a3a3a  guisp=NONE
  highlight lCursor           cterm=NONE       ctermfg=NONE  ctermbg=NONE  gui=NONE       guifg=NONE     guibg=NONE     guisp=NONE

  highlight CurSearch         cterm=NONE       ctermfg=237   ctermbg=68    gui=bold       guifg=#3a3a3a  guibg=#5f87d7  guisp=NONE
  highlight debugPC           cterm=reverse    ctermfg=NONE  ctermbg=176   gui=NONE       guifg=NONE     guibg=#d787d7  guisp=NONE
  highlight debugBreakpoint   cterm=reverse    ctermfg=NONE  ctermbg=210   gui=NONE       guifg=NONE     guibg=#ff8787  guisp=NONE
  highlight ToolbarLine       cterm=NONE       ctermfg=NONE  ctermbg=NONE  gui=NONE       guifg=NONE     guibg=NONE     guisp=NONE
  highlight ToolbarButton     cterm=reverse    ctermfg=NONE  ctermbg=NONE  gui=reverse    guifg=#9e9e9e  guibg=#1c1c1c  guisp=NONE
  "" FULL-FEATURED }}}
elseif &t_Co == 8 || $TERM !~# '^linux' || &t_Co == 16
  """ 16-COLOR PALETTE {{{
  set t_Co=16

  " Syntax groups: convenient highlight groups for syntax highlighting
  highlight Standard       cterm=NONE  ctermfg=7  ctermbg=NONE
  highlight Normal         cterm=NONE  ctermfg=7  ctermbg=0
  highlight TextRed        cterm=NONE  ctermfg=1  ctermbg=NONE
  highlight TextGreen      cterm=NONE  ctermfg=2  ctermbg=NONE
  highlight TextYellow     cterm=NONE  ctermfg=3  ctermbg=NONE
  highlight TextBlue       cterm=NONE  ctermfg=4  ctermbg=NONE
  highlight TextMagenta    cterm=NONE  ctermfg=5  ctermbg=NONE
  highlight TextCyan       cterm=NONE  ctermfg=6  ctermbg=NONE
  highlight TextWhite      cterm=NONE  ctermfg=7  ctermbg=NONE
  highlight TextLightGray  cterm=NONE  ctermfg=8  ctermbg=NONE
  " We only get 1 gray on a 16-color palette: color 8
  highlight! link  TextDarkGray  TextLightGray

  " User highlight groups
  highlight User1  cterm=NONE  ctermfg=1  ctermbg=8
  highlight User2  cterm=NONE  ctermfg=2  ctermbg=8
  highlight User3  cterm=NONE  ctermfg=3  ctermbg=8
  highlight User4  cterm=NONE  ctermfg=4  ctermbg=8
  highlight User5  cterm=NONE  ctermfg=5  ctermbg=8
  highlight User6  cterm=NONE  ctermfg=6  ctermbg=8
  highlight User7  cterm=NONE  ctermfg=7  ctermbg=8

  " Editor chrome
  highlight Bold             cterm=bold      ctermfg=NONE ctermbg=NONE
  highlight ColorColumn      cterm=NONE      ctermfg=NONE ctermbg=8
  highlight Conceal          cterm=NONE      ctermfg=4    ctermbg=0
  highlight Cursor           cterm=NONE      ctermfg=0    ctermbg=7
  highlight CursorColumn     cterm=NONE      ctermfg=NONE ctermbg=8
  highlight CursorLine       cterm=NONE      ctermfg=NONE ctermbg=8
  highlight CursorLineNr     cterm=NONE      ctermfg=7    ctermbg=0
  highlight Debug            cterm=NONE      ctermfg=1    ctermbg=NONE
  highlight DiffAdd          cterm=bold      ctermfg=2    ctermbg=8
  highlight DiffChange       cterm=NONE      ctermfg=7    ctermbg=8
  highlight DiffDelete       cterm=NONE      ctermfg=1    ctermbg=8
  highlight DiffText         cterm=bold      ctermfg=3    ctermbg=8
  highlight Directory        cterm=NONE      ctermfg=4    ctermbg=NONE
  highlight EndOfBuffer      cterm=NONE      ctermfg=7    ctermbg=NONE
  highlight Error            cterm=NONE      ctermfg=0    ctermbg=1
  highlight ErrorMsg         cterm=NONE      ctermfg=1    ctermbg=0
  highlight FoldColumn       cterm=NONE      ctermfg=7    ctermbg=0
  highlight Folded           cterm=NONE      ctermfg=7    ctermbg=8
  highlight Ignore           cterm=NONE      ctermfg=0    ctermbg=NONE
  highlight IncSearch        cterm=NONE      ctermfg=8    ctermbg=7
  highlight LineNr           cterm=NONE      ctermfg=7    ctermbg=0
  highlight Macro            cterm=NONE      ctermfg=1    ctermbg=NONE
  highlight MatchParen       cterm=NONE      ctermfg=NONE ctermbg=8
  highlight ModeMsg          cterm=bold      ctermfg=2    ctermbg=NONE
  highlight MoreMsg          cterm=NONE      ctermfg=2    ctermbg=NONE
  highlight NonText          cterm=NONE      ctermfg=7    ctermbg=NONE
  highlight Pmenu            cterm=NONE      ctermfg=7    ctermbg=8
  highlight PmenuSbar        cterm=NONE      ctermfg=NONE ctermbg=8
  highlight PmenuSel         cterm=NONE      ctermfg=3    ctermbg=8
  highlight PmenuThumb       cterm=NONE      ctermfg=NONE ctermbg=8
  highlight Question         cterm=NONE      ctermfg=4    ctermbg=NONE
  highlight QuickFixLine     cterm=underline ctermfg=NONE ctermbg=NONE
  highlight Search           cterm=NONE      ctermfg=8    ctermbg=7
  highlight SignColumn       cterm=NONE      ctermfg=8    ctermbg=0
  highlight SpecialKey       cterm=NONE      ctermfg=7    ctermbg=NONE
  highlight SpellBad         cterm=underline ctermfg=7    ctermbg=0
  highlight SpellCap         cterm=NONE      ctermfg=NONE ctermbg=0
  highlight SpellLocal       cterm=NONE      ctermfg=NONE ctermbg=0
  highlight SpellRare        cterm=NONE      ctermfg=NONE ctermbg=0
  highlight StatusLine       cterm=NONE      ctermfg=7    ctermbg=8
  highlight StatusLineNC     cterm=NONE      ctermfg=7    ctermbg=8
  highlight StatusLineTerm   cterm=NONE      ctermfg=7    ctermbg=8
  highlight StatusLineTermNC cterm=NONE      ctermfg=7    ctermbg=8
  highlight TabLine          cterm=NONE      ctermfg=7    ctermbg=8
  highlight TabLineFill      cterm=NONE      ctermfg=8    ctermbg=8
  highlight TabLineSel       cterm=NONE      ctermfg=3    ctermbg=8
  highlight Title            cterm=NONE      ctermfg=4    ctermbg=NONE
  highlight TooLong          cterm=NONE      ctermfg=1    ctermbg=NONE
  highlight Underlined       cterm=underline ctermfg=1    ctermbg=NONE
  highlight VertSplit        cterm=NONE      ctermfg=0    ctermbg=0
  highlight Visual           cterm=NONE      ctermfg=NONE ctermbg=8
  highlight VisualNOS        cterm=NONE      ctermfg=1    ctermbg=NONE
  highlight WarningMsg       cterm=NONE      ctermfg=1    ctermbg=NONE
  highlight WildMenu         cterm=NONE      ctermfg=3    ctermbg=8
  highlight lCursor          cterm=NONE      ctermfg=NONE ctermbg=NONE

  highlight CurSearch        cterm=NONE      ctermfg=8    ctermbg=4
  highlight debugPC          cterm=reverse   ctermfg=NONE ctermbg=5
  highlight debugBreakpoint  cterm=reverse   ctermfg=NONE ctermbg=1
  highlight ToolbarLine      cterm=NONE      ctermfg=NONE ctermbg=NONE
  highlight ToolbarButton    cterm=reverse   ctermfg=NONE ctermbg=NONE
  """ END 16-COLOR PALETTE }}}
endif

""" TERMINAL ANSI COLORS {{{
if (has('termguicolors') && &termguicolors) || has('gui_running')
  let g:terminal_ansi_colors = [
        \ '#242424',
        \ '#ff8787',
        \ '#87d787',
        \ '#ffd75f',
        \ '#5f87d7',
        \ '#d787d7',
        \ '#5fd7d7',
        \ '#d0d0d0',
        \ '#767676',
        \ '#ff8787',
        \ '#87d787',
        \ '#ffd75f',
        \ '#5f87d7',
        \ '#d787d7',
        \ '#5fd7d7',
        \ '#d0d0d0',
        \ ]
endif
""" END TERMINAL ANSI COLORS }}}
""" END ENVIRONMENT-DEPENDENT HIGHLIGHTS }}}

""" NON-FT-SPECIFIC SYNTAX {{{
" Linked Highlight groups
highlight! link CursorLineFold FoldColumn
highlight! link CursorLineSign SignColumn
highlight! link LineNrAbove    LineNr
highlight! link LineNrBelow    LineNr

" Base groups: only these groups + Normal will form the foundation of syntax highlighting
highlight! link  Comment  TextLightGray
highlight! link  String   TextGreen
highlight! link  Special  TextCyan
highlight! link  Todo     TextYellowU

" Linked groups:
highlight! link  Boolean       Standard
highlight! link  Conditional   Standard
highlight! link  Constant      Standard
highlight! link  Define        Standard
highlight! link  Delimiter     Standard
highlight! link  Exception     Standard
highlight! link  Float         Standard
highlight! link  Function      Standard
highlight! link  Identifier    Standard
highlight! link  Include       Standard
highlight! link  Keyword       Standard
highlight! link  Label         Standard
highlight! link  Macro         Standard
highlight! link  Number        Standard
highlight! link  Operator      Standard
highlight! link  PreProc       Standard
highlight! link  Repeat        Standard
highlight! link  Statement     Standard
highlight! link  StorageClass  Standard
highlight! link  Structure     Standard
highlight! link  Type          Standard
highlight! link  Typedef       Standard
highlight! link  Character     String
highlight! link  SpecialChar   Special
" highlight! link  Tag           Todo
highlight! link  Tag           TextYellow

" Clear italic, I don't like it
" highlight clear Italic
""" END NON-FT-SPECIFIC SYNTAX }}}

""" FT-SPECIFIC SYNTAX {{{
"" Mostly just making sure straggler highlight groups are linked back to Normal
"" With a few exceptions getting their own colors

" Diff
" Color is natural for diffs
highlight! link  DiffFile     TextCyan
highlight! link  DiffLine     TextBlue
highlight! link  DiffAdded    TextGreen
highlight! link  DiffNewFile  TextGreen
highlight! link  DiffRemoved  TextRed

" Git
" Color is useful for Git commits, where certain information must stick out of a sea of text
" Color is also useful for the "diff-like" highlight groups, such as Selected, Discarded, etc
highlight! link  gitcommitComment        Comment
highlight! link  gitcommitUntracked      Comment
highlight! link  gitcommitDiscarded      Comment
highlight! link  gitcommitSelected       Comment
highlight! link  gitcommitHeader         Comment
highlight! link  gitcommitOverflow       TextRed
highlight! link  gitcommitBranch         TextCyan
highlight! link  gitcommitSummary        TextGreen
highlight! link  gitcommitSelectedType   TextGreen
highlight! link  gitcommitSelectedFile   TextGreen
highlight! link  gitcommitUnmergedType   TextRed
highlight! link  gitcommitUnmergedFile   TextRed
highlight! link  gitcommitDiscardedFile  TextRed
highlight! link  gitcommitDiscardedType  TextRed
highlight! link  gitcommitUntrackedFile  TextRed

" Fugitive
" Stick to Git colors as close as possible
highlight! link  diffSubname                Comment
highlight! link  fugitiveHash               Comment
highlight! link  fugitiveModifier           TextRed
highlight! link  fugitiveUnstagedModifier   TextRed
highlight! link  fugitiveUntrackedModifier  Comment
highlight! link  fugitiveStagedModifier     TextGreen

" HTML
highlight! link  htmlBold            Standard
highlight! link  htmlItalic          Standard
highlight! link  htmlArg             Standard
highlight! link  htmlEndTag          Standard
highlight! link  htmlSpecialChar     Standard
highlight! link  htmlSpecialTagName  Standard
highlight! link  htmlTag             Standard
highlight! link  htmlTagName         Standard
highlight! link  htmlTitle           Standard
highlight! link  htmlH1              Standard
highlight! link  htmlH2              Standard
highlight! link  htmlH3              Standard
highlight! link  htmlH4              Standard
highlight! link  htmlH5              Standard
highlight! link  htmlH6              Standard
highlight! link  htmlLink            Special

" Pug
highlight! link  pugClassChar     Standard
highlight! link  pugClass         Standard
highlight! link  pugIdChar        Standard
highlight! link  pugPipeChar      Standard
highlight! link  pugTagBlockChar  Standard

" CSS
highlight! link  cssAttr            Standard
highlight! link  cssAttrComma       Standard
highlight! link  cssBraces          Standard
highlight! link  cssClassName       Standard
highlight! link  cssColor           Standard
highlight! link  cssImportant       Standard
highlight! link  cssKeyFrameProp    Standard
highlight! link  cssProp            Standard
highlight! link  cssPseudoClassId   Standard
highlight! link  cssUnitDecorators  Standard
highlight! link  cssValueLength     Standard
highlight! link  cssValueNumber     Standard
highlight! link  cssValueTime       Standard
highlight! link  cssVendor          Standard

" JavaScript
" Highlighting directives in JSDoc/ESDoc comments is useful
highlight! link  jsDocTags            TextBlue
highlight! link  javaScript           Standard
highlight! link  javaScriptBraces     Standard
highlight! link  jsArrowFunction      Standard
highlight! link  jsBuiltins           Standard
highlight! link  jsCatch              Standard
highlight! link  jsClassDefinition    Standard
highlight! link  jsClassFuncName      Standard
highlight! link  jsDocParam           Standard
highlight! link  jsDocType            Standard
highlight! link  jsDocTypeNoParam     Standard
highlight! link  jsException          Standard
highlight! link  jsExceptions         Standard
highlight! link  jsFuncCall           Standard
highlight! link  jsGlobalNodeObjects  Standard
highlight! link  jsGlobalObjects      Standard
highlight! link  jsNull               Standard
highlight! link  jsObjectKey          Standard
highlight! link  jsPrototype          Standard
highlight! link  jsRegexpQuantifier   Standard
highlight! link  jsRegexpString       Standard
highlight! link  jsReturn             Standard
highlight! link  jsSpecial            Standard
highlight! link  jsSuper              Standard
highlight! link  jsTaggedTemplate     Standard
highlight! link  jsThis               Standard
highlight! link  jsTry                Standard
highlight! link  jsUndefined          Standard

" JSX
highlight! link  typescriptStringS      String
highlight! link  typescriptComment      Comment
highlight! link  typescriptLineComment  Comment
highlight! link  typescriptDocComment   Comment
highlight! link  typescriptCommentTodo  Todo
highlight! link  jsxBraces              Standard
highlight! link  jsxOpenPunct           Standard
highlight! link  jsxCloseString         Standard

" TypeScript
highlight! link  typescriptDecorators     Standard
highlight! link  typescriptExceptions     Standard
highlight! link  typescriptExport         Standard
highlight! link  typescriptGlobalObjects  Standard
highlight! link  typescriptImport         Standard
highlight! link  typescriptTry            Standard

" Shell
" Highlighting parameter expansions within a string is useful
highlight! link  shDeref         TextCyan
highlight! link  shCmdSubRegion  Standard
highlight! link  shCommandSub    Standard
highlight! link  shOption        Standard

" Mail
" Highlighting different quote levels is useful
highlight! link  mailQuoted1  TextYellow
highlight! link  mailQuoted2  TextGreen
highlight! link  mailQuoted3  TextMagenta
highlight! link  mailQuoted4  TextCyan
highlight! link  mailQuoted5  TextBlue
highlight! link  mailQuoted6  TextYellow
highlight! link  mailEmail    TextBlue
highlight! link  mailURL      TextBlue

" Markdown
" Highlighting headings and code blocks is useful
highlight! link  markdownError             Standard
highlight! link  markdownCode              TextGreen
highlight! link  markdownCodeBlock         TextGreen
highlight! link  markdownHeadingDelimiter  Title
highlight! link  markdownH1                Title
highlight! link  markdownH2                Title
highlight! link  markdownH3                Title

" Vim
" Highlighting conventional comment titles (e.g. Author, etc) is useful
highlight! link  vimCommentTitle    TextYellow
highlight! link  vimAddress         Standard
highlight! link  vimAutoEvent       Standard
highlight! link  vimBracket         Standard
highlight! link  vimContinue        Standard
highlight! link  vimEnvvar          Standard
highlight! link  vimFuncSID         Standard
highlight! link  vimGroup           Standard
highlight! link  vimHiAttrib        Standard
highlight! link  vimHiGroup         Standard
highlight! link  vimHiGuiRgb        Standard
highlight! link  vimHiNmbr          Standard
highlight! link  vimHiTerm          Standard
highlight! link  vimMapMod          Standard
highlight! link  vimMapModKey       Standard
highlight! link  vimNotation        Standard
highlight! link  vimOption          Standard
highlight! link  vimPatSep          Standard
highlight! link  vimSetSep          Standard
highlight! link  vimSubstFlags      Standard
highlight! link  vimSynOption       Standard
highlight! link  vimSynReg          Standard
highlight! link  vimUserAttrb       Standard
highlight! link  vimUserAttrbCmplt  Standard

" Vim Help
" Highlighting tags you can <C-]> on is useful
highlight! link  helpHyperTextJump  TextMagenta
highlight! link  helpHeadLine       Title
highlight! link  helpExample        Comment

" Netrw
" Stick to ls colors as close as possible
highlight! link  netrwExe  TextGreen

" Kickfix
" Zebra highlighting in the quickfix is useful
highlight! link  qfFileName1  TextBlue
highlight! link  qfFileName2  TextCyan
highlight! link  qfZebra1     Standard
highlight! link  qfZebra2     Standard

" Vimwiki
" Highlighting links you can <CR> on is useful
highlight! link  VimwikiCode  TextMagenta
highlight! link  VimwikiLink  TextMagenta
highlight! link  VimwikiPre   Standard

" Tmux
highlight! link  tmuxKey  Standard

" i3
highlight! link  i3ConfigVariableModifier  Standard
highlight! link  i3ConfigString            String

" selectresults
highlight! link  SelectMatched             TextBlue

""" END FT-SPECIFIC SYNTAX }}}

" Plugins: {{{
highlight! link TelescopeMatching TextYellowU
highlight! link TelescopePromptPrefix TextYellow
highlight! link TelescopeSelection PmenuSel
" }}}

" vim: fdm=marker:nowrap:tw=0
