let g:startify_session_sort = 1
let g:startify_change_to_vcs_root = 1
let g:startify_update_oldfiles = 0

if has('nvim')
  let g:startify_session_dir=stdpath('data').'\sessions'

  let g:startify_skiplist = [
        \ '.*\\share\\nvim\\runtime\\doc\\.*',
        \ '.*\\nvim-data\\.*\\doc\\.*',
        \ ]
else
  let g:startify_session_dir=expand("$USERPROFILE").'\vimfiles\sessions'

  let g:startify_skiplist = [
        \ '.*\\vim.*\\doc\\.*',
        \ '.*\\vimfiles\\.*\\doc\\.*',
        \ ]
endif

let g:startify_custom_header = []
let g:startify_bookmarks = [
      \ {'c': expand("$MYVIMRC") }
      \ ]

let g:startify_files_number = 20

let g:startify_commands = [
      \ {'x': 'terminal'},
      \ ]

      " \ { 'type': 'dir',       'header': ['   MRU '.getcwd()]  },
let g:startify_lists = [
      \ { 'type': 'sessions',  'header': ['   Sessions']       },
      \ { 'type': 'files',     'header': ['   MRU']            },
      \ { 'type': 'bookmarks', 'header': ['   Bookmarks']      },
      \ { 'type': 'commands',  'header': ['   Commands']       },
      \ ]
