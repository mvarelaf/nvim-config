nnoremap <buffer> <leader>c :cclose<cr>
nnoremap <buffer> <F3> :cclose<cr>

if get(w:, 'quickfix_title') =~# 'Help TOC'
  nnoremap <buffer> <leader>c :lclose<cr>
  nnoremap <buffer> <F3> :lclose<cr>
endif
