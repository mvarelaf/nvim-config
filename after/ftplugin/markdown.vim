" https://25.wf/posts/2020-09-04-vim-markdown-text-object.html
" con plugins https://github.com/coachshea/vim-textobj-markdown usa https://github.com/kana/vim-textobj-user

" function! s:inCodeFence()
"     " Search backwards for the opening of the code fence.
" 	call search('^```.*$', 'bceW')
"     " Move one line down
" 	normal! j
"     " Move to the begining of the line at start selecting
" 	normal! 0v
"     " Search forward for the closing of the code fence.
" 	call search("```", 'ceW')

" 	normal! kg_
" endfunction

" function! s:aroundCodeFence()
"     " Search backwards for the opening of the code fence.
" 	call search('^```.*$', 'bcW')
"" 	normal! v$
" 	normal! V$
"     " Search forward for the closing of the code fence.
" 	call search('```', 'eW')
" endfunction

" xnoremap <buffer> <silent> if :call <sid>inCodeFence()<cr>
" onoremap <buffer> <silent> if :call <sid>inCodeFence()<cr>
" xnoremap <buffer> <silent> af :call <sid>aroundCodeFence()<cr>
" onoremap <buffer> <silent> af :call <sid>aroundCodeFence()<cr>

" **********************************************************
" alternativa en https://jdhao.github.io/2020/11/15/nvim_text_objects/

vnoremap <buffer> <silent> if :<C-U>call <SID>MdCodeBlockTextObj('i')<CR>
onoremap <buffer> <silent> if :<C-U>call <SID>MdCodeBlockTextObj('i')<CR>

vnoremap <buffer> <silent> af :<C-U>call <SID>MdCodeBlockTextObj('a')<CR>
onoremap <buffer> <silent> af :<C-U>call <SID>MdCodeBlockTextObj('a')<CR>

function! s:MdCodeBlockTextObj(type) abort
  " the parameter type specify whether it is inner text objects or around
  " text objects.

  " Move the cursor to the end of line in case that cursor is on the opening
  " of a code block. Actually, there are still issues if the cursor is on the
  " closing of a code block. In this case, the start row of code blocks would
  " be wrong. Unless we can match code blocks, it is not easy to fix this.
  normal! $
  let start_row = searchpos('\s*```', 'bnW')[0]
  let end_row = searchpos('\s*```', 'nW')[0]

  let buf_num = bufnr()
  " For inner code blocks, remove the start and end line containing backticks.
  if a:type ==# 'i'
    let start_row += 1
    let end_row -= 1
  endif
  " echo a:type start_row end_row

  call setpos("'<", [buf_num, start_row, 1, 0])
  call setpos("'>", [buf_num, end_row, 1, 0])
  execute 'normal! `<V`>'
endfunction

function! s:show_toc() abort
  let bufname = bufname('%')
  let info = getloclist(0, {'winid': 1})
  if !empty(info) && getwinvar(info.winid, 'qf_toc') ==# bufname
    lopen
    return
  endif

  let toc = []
  let lnum = 2
  let last_line = line('$') - 1
  let last_added = 0
  let has_section = 0
  let has_sub_section = 0

  while lnum && lnum <= last_line
    let level = 0
    let add_text = ''
    let text = getline(lnum)

    " if text =~# '^=\+$' && lnum + 1 < last_line
    if text =~# '^#\+\s\+' && lnum + 1 < last_line
      " A de-facto section heading.  Other headings are inferred.
      let has_section = 1
      let has_sub_section = 0
      " let lnum = nextnonblank(lnum + 1)
      let text = getline(lnum)
      let add_text = text
      while add_text =~# '\*[^*]\+\*\s*$'
        let add_text = matchstr(add_text, '.*\ze\*[^*]\+\*\s*$')
      endwhile
    " elseif text =~# '^[A-Z0-9][-A-ZA-Z0-9 .][-A-Z0-9 .():]*\%([ \t]\+\*.\+\*\)\?$'
    "   " Any line that's yelling is important.
    "   let has_sub_section = 1
    "   let level = has_section
    "   let add_text = matchstr(text, '.\{-}\ze\s*\%([ \t]\+\*.\+\*\)\?$')
    elseif text =~# '\~$'
          \ && matchstr(text, '^\s*\zs.\{-}\ze\s*\~$') !~# '\t\|\s\{2,}'
          \ && getline(lnum - 1) =~# '^\s*<\?$\|^\s*\*.*\*$'
          \ && getline(lnum + 1) =~# '^\s*>\?$\|^\s*\*.*\*$'
      " These lines could be headers or code examples.  We only want the
      " ones that have subsequent lines at the same indent or more.
      let l = nextnonblank(lnum + 1)
      if getline(l) =~# '\*[^*]\+\*$'
        " Ignore tag lines
        let l = nextnonblank(l + 1)
      endif

      if indent(lnum) <= indent(l)
        let level = has_section + has_sub_section
        let add_text = matchstr(text, '\S.*')
      endif
    endif

    let add_text = substitute(add_text, '\s\+$', '', 'g')
    if !empty(add_text) && last_added != lnum
      let last_added = lnum
      call add(toc, {'bufnr': bufnr('%'), 'lnum': lnum,
            \ 'text': repeat('  ', level) . add_text})
    endif
    let lnum = nextnonblank(lnum + 1)
  endwhile

  call setloclist(0, toc, ' ')
  call setloclist(0, [], 'a', {'title': 'Help TOC'})
  lopen
  let w:qf_toc = bufname
endfunction

nnoremap <silent><buffer> gO :call <sid>show_toc()<cr>
