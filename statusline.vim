function! Statusline_encoding() abort
  return &fileencoding == 'utf-8'?"":"[".&fileencoding."]"
endfunction

function! Statusline_fileformat() abort
  if ( has('win32') || has('win64') ) && &fileformat == 'dos'
    return ""
  elseif has('linux') && &fileformat == 'unix'
    return ""
  else
    if &fileformat == 'dos'
      return ''  "-- e70f
    elseif &fileformat == 'unix'
      return '' " -- e712
    elseif &fileformat == 'mac'
      return ''  "-- e711
    endif
  endif
endfunction

" fileformat without symbols
" function! Statusline_fileformat() abort
"   if has('win32') || has('win64')
"     return &fileformat == 'dos'?"":"[".&fileformat."]"
"   elseif has('linux')
"     return &fileformat == 'unix'?"":"[".&fileformat."]"
"   else
"       return "[".&fileformat."]"
"   endif
" endfunction

function! Statusline_time() abort
  return strftime("%A %d %B %T")
endfunction

function! TrailingSpaceWarning() abort
  if &modifiable
    if !exists("b:statusline_trailing_space_warning")
      let lineno = search('\s$', 'nw')
      if lineno != 0
        let b:statusline_trailing_space_warning = '[trailing:'.lineno.']'
      else
        let b:statusline_trailing_space_warning = ''
      endif
    endif
  else
    let b:statusline_trailing_space_warning = ''
  endif
  return b:statusline_trailing_space_warning
endfunction

if has('autocmd')
  " recalculate when idle, and after saving
  augroup statusline_trail
    autocmd!
    autocmd CursorHold,BufWritePost * unlet! b:statusline_trailing_space_warning
    autocmd VimEnter,WinEnter,BufNew,BufNewFile,TabNewEntered * unlet! b:statusline_trailing_space_warning
  augroup END

  augroup StatusLineAug
    autocmd!
    autocmd VimEnter,WinEnter,TermOpen * call Statusline_update('active')
    autocmd BufDelete,BufNew,BufNewFile * call Statusline_update('active')
    autocmd FileType,TabNewEntered,CursorHold * call Statusline_update('active')
    autocmd WinLeave * call Statusline_update('deactive')
  augroup END
endif

set noshowmode
let g:currentmode={
    \ 'n'  : ' ',
    \ 'no' : ' ',
    \ 'v'  : ' ',
    \ 'V'  : ' ',
    \ '' : ' ',
    \ 's'  : 'S',
    \ 'S'  : 'S',
    \ '' : 'S',
    \ 'i'  : '%#ModeMsg#INSERT',
    \ 'R'  : '%#ModeMsg#R',
    \ 'Rv' : '%#ModeMsg#R',
    \ 'c'  : ' ',
    \ 'cv' : ' ',
    \ 'ce' : ' ',
    \ 'r'  : 'P',
    \ 'rm' : 'P',
    \ 'r?' : 'P',
    \ '!'  : 'SHELL',
    \ 't'  : 'TERMINAL'
    \}

" vim mode
function! Statusline_mode(...) abort
  return get(a:, '1', g:currentmode[mode()])
endfunction

" set current buffer statusline
function! Statusline_update(...) abort
  if &filetype ==# '' && &buftype ==# 'nofile'
    return
  endif
  let l:type = get(a:, '1', 'active')
  let l:status = ''
  " get statusline of filetype from g:statusline
  if exists('g:statusline["' . &filetype . '"]')
    let l:status = get(g:statusline[&filetype], l:type, v:false)
  else
    let l:status = get(g:statusline['_'], l:type, v:false)
  endif
  if l:status !=# v:false
    execute 'setlocal statusline=' . l:status
  endif
endfunction

if !exists('g:statusline')
  let g:statusline = {}
  let g:statusline["startify"] = {
    \   'active': '\ %{hostname()}\ %{Statusline_time()}%=\ %Y\ ',
    \   'deactive': '%Y'
    \ }
endif

if !exists('g:statusline["_"]')
  let g:statusline['_'] = {
    \   'active': join([
    \         '%{%Statusline_mode()%}%*',
    \         '\ %#TabLineSel#%{tabpagewinnr(tabpagenr())}%*',
    \         '\ %<%f%#Error#%m%*%r%w\',
    \         '%=',
    \         '%{Statusline_encoding()}%{Statusline_fileformat()}',
    \         '\ %{FugitiveHead(8)}',
    \         '\ %Y',
    \         '\ %l/%L,%02v\ %p%%\ ',
    \         '%#ErrorMsg#%{TrailingSpaceWarning()}'
    \      ], ''),
    \   'deactive': join([
    \         '\ \ \%{tabpagewinnr(tabpagenr())}',
    \         '\ %<%f\ %#ErrorMsg#%{and(&modifiable,&modified)?\"[+]\":\"\"}%*'
    \      ], '')
    \ }
endif
