if exists('g:nvy')
  set guifont=Cousine\ Nerd\ Font\ Mono:h10
endif

if exists('g:GuiLoaded')
  " GuiFont! SauceCodePro NF:h9,Consolas:h10
  " GuiFont! Victor Mono:h10
  GuiFont! Cousine\ NF:h10
endif

" Non tracked local configuration
if filereadable(stdpath('config')."\\ginit_local.vim")
  execute ":source " . stdpath('config')."\\ginit_local.vim"
endif

