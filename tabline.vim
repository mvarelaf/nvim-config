" Adapted from https://stackoverflow.com/questions/5927952/whats-the-implementation-of-vims-default-tabline-function

if exists("+showtabline")
    function! GenerateTabLine()
        let s = ''
        let t = tabpagenr()
        let i = 1
        while i <= tabpagenr('$')
            let buflist = tabpagebuflist(i)
            let winnr = tabpagewinnr(i)
            let s .= '%' . i . 'T'
            let s .= (i == t ? '%5*' : '%4*')
            let s .= ' '
            let s .= i . ':'
            let s .= (i == t ? '%#TabLineSel#' : '%#TabLine#')
            "let s .= winnr . '/' . tabpagewinnr(i,'$')
            let file = bufname(buflist[winnr - 1])
            let file = fnamemodify(file, ':p:t')
            "let file = fnamemodify(file, ':~:.')
            if file == ''
                let file = '[No Name]'
            endif
            let s .= ' ' . file
            "let mod = '%6*'
            let mod = ''
            let j = 1
            while j <= tabpagewinnr(i,'$')
                if getbufvar(buflist[j - 1], "&modified") != 0
                    "let mod .= '%#StatusLineModFlag#*'
                    let mod .= '[+]'
                    break
                endif
                let j = j + 1
            endwhile
            let s .= mod
            let s .= ' %*'
            let i = i + 1
        endwhile
        let s .= '%T%#TabLineFill#%='
        let s .= (tabpagenr('$') > 1 ? '%999XX' : 'X')
        return s
    endfunction
endif

if exists("+showtabline")
    " 0, 1 or 2; when to use a tab pages line
    set showtabline=1
    " custom tab pages line
    set tabline=%!GenerateTabLine()
endif
